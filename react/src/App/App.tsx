import * as React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Stopwatch from "../Stopwatch/Stopwatch";
import Competition from "../Competition/Competition"
import 'bootstrap/dist/css/bootstrap.min.css';
import * as openSocket from "socket.io-client";
import { Provider } from 'react-redux';
import store from '../redux/store';
import './App.css'
import Ranking from 'src/rank/Ranking';
import { reactRoom } from 'src/redux/action';


class App extends React.Component {

	// join the room for react int the socket
	public componentDidMount(){
        reactRoom();
    }

	public render() {
		return (
			<div id="App">
				<Provider store={store}>
					<BrowserRouter>
				 		<Switch>
					 		<Route path="/stopwatch" component={Stopwatch} />
							<Route path="/competition/:player" component={Competition} />
							<Route path="/ranking" component={Ranking} />
						</Switch>
					</BrowserRouter> 
				</Provider>
			</div>
    	)
	}
}

export const clientSocket = openSocket(`${process.env.REACT_APP_API_SERVER}`);

export default App;
