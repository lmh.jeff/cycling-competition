import { ThunkResult } from '../store';
import { Dispatch } from 'redux';
import IRankingState from './state';
import { clientSocket } from 'src/App/App';

type RANKING_ACTION = "RANKING_ACTION";

interface IRANKINGACTION {
    type: RANKING_ACTION,
    rankingList: IRankingState
}

export type RankingAction = IRANKINGACTION;

export const getRanking = (): ThunkResult<void> => {
    return (dispatch: Dispatch<RankingAction>) => {
        clientSocket.on("rank", (top10AndMyself : IRankingState) => {
            dispatch(ranking(top10AndMyself));
        })
    }
}

export function ranking(rankingList : IRankingState): IRANKINGACTION {
    return {
        type: "RANKING_ACTION",
        rankingList
    }
}
