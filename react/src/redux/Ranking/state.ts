export enum Player1Or2 {
    Player1 = "1",
    Player2 = "2"
}

export interface ISingleRanking {
    id? : number,
    PlayerId : number,
    Distance : number,
    Date : Date,
    Rank? : number
}

interface IRankingState {
    player1Ranking : ISingleRanking[],
    player2Ranking : ISingleRanking[]
}

export default IRankingState;