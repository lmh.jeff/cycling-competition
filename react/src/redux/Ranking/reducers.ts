
import { RankingAction } from './actions';
import IRankingState from './state';

const initialState : IRankingState = {
    player1Ranking : new Array(),
    player2Ranking : new Array()
}

// get the ranking from server
const rankingReducer = (oldState : IRankingState = initialState, action: RankingAction) : IRankingState => {
    switch (action.type) {
        // press the start button
        case "RANKING_ACTION" : {
            const newState: IRankingState = Object.assign({}, action.rankingList);
            return newState;
        }
        default : 
            return oldState;
    }
}

export default rankingReducer;


