import { ThunkResult } from '../store';
import { clientSocket } from '../../App/App';
import { Dispatch } from 'redux';

type PLAYERID = 'PLAYER_ID';
type PLAYERDISTANCE = 'PLAYER_DISTANCE';
type PLAYERSPEED = 'PLAYER_SPEED';
type STARTCYCLING = 'START_CYCLING';
type RESETDISTANCE = 'RESET_DISTANCE';

interface IPLAYERID {
    type : PLAYERID,
    id : number[]
}
interface IPLAYERDISTANCE {
    type : PLAYERDISTANCE,
    distance : number[]
}

interface IPLAYERSPEED {
    type : PLAYERSPEED,
    speed : number[]
}

interface ISTARTCYCLING {
    type : STARTCYCLING
}

interface IRESETDISTANCE {
    type : RESETDISTANCE
}

export type DistanceAction = IPLAYERID | IPLAYERDISTANCE | IPLAYERSPEED | ISTARTCYCLING | IRESETDISTANCE;

export const getDistanceState = () : ThunkResult<void> => {
    return (dispatch : Dispatch<DistanceAction>) => {
        clientSocket.on("PlayerId", (id : number[]) => {
            dispatch(setId(id));
        })    
        clientSocket.on("PlayerDistance", (distance : number[]) => {
            dispatch(setDistance(distance));
        })
        clientSocket.on("PlayerSpeed",(speed : number[]) => {
            dispatch(setSpeed(speed));
        })
        clientSocket.on("StartCycling",() => {
            dispatch(startCycling());
        })
        clientSocket.on("ResetDistance",() => {
            dispatch(resetDistance());
        })
    }
}

// send a request to the server to gen the new playerid
export const requestPlayerId = () => {
    clientSocket.emit("requestPlayerId");
}

export const setId = (id : number[]) : IPLAYERID => {
    return {
        type : "PLAYER_ID",
        id
    }
}

export const setDistance = (distance : number[]) : IPLAYERDISTANCE => {
    return {
        type : "PLAYER_DISTANCE",
        distance
    }
}

export const setSpeed = (speed : number[]) : IPLAYERSPEED => {
    return {
        type : "PLAYER_SPEED",
        speed
    }
}

export const startCycling = () : ISTARTCYCLING => {
    return {
        type : "START_CYCLING"
    }
}

export const resetDistance = () : IRESETDISTANCE => {
    return {
        type : "RESET_DISTANCE"
    }
}
