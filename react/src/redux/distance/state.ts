export interface IDistanceState {
    Player1Id : number | null,
    Player2Id : number | null,
    Player1TotalDistance : number,
    Player2TotalDistance : number,
    Player1Speed : number,
    Player2Speed : number,
    isStopped : boolean
}

export default IDistanceState;