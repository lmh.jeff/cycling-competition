import IDistanceState from './state'
import { DistanceAction } from './actions'

const initialState : IDistanceState = {
    Player1Id : null,
    Player2Id : null,
    Player1TotalDistance : 0,
    Player2TotalDistance : 0,
    Player1Speed : 0,
    Player2Speed : 0,
    isStopped : true
}

const distanceReducer = (oldState : IDistanceState = initialState, action : DistanceAction) : IDistanceState => {
    switch(action.type) {
        // write the two player id 
        case "PLAYER_ID" : {
            const newState : IDistanceState = Object.assign({}, oldState, 
                { Player1Id : action.id[0], Player2Id : action.id[1] });
            return newState;
        }
        // write the two player distance
        case "PLAYER_DISTANCE" : {
            const newState : IDistanceState = Object.assign({}, oldState, 
                { Player1TotalDistance : action.distance[0], Player2TotalDistance : action.distance[1] });
            return newState;  
        }  
        // write two player speed
        case "PLAYER_SPEED" : {
            const newState : IDistanceState = Object.assign({},oldState,
                { Player1Speed : action.speed[0], Player2Speed : action.speed[1] });
            return newState;
        }    
        // start the biking animation
        case "START_CYCLING" : {
            const newState : IDistanceState = Object.assign({}, oldState, { isStopped : false });
            return newState;
        }
        // reset the distance and send the bike back to the starting point
        case "RESET_DISTANCE" : {
            const newState : IDistanceState = Object.assign({}, oldState, 
                {Player1TotalDistance : 0, Player2TotalDistance : 0, isStopped : true})
            return newState;
        }
        default : 
            return oldState;
    }
}

export default distanceReducer;