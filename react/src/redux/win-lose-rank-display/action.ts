import { ThunkResult } from '../store';
import { Dispatch } from 'redux';
import { clientSocket } from '../../App/App';

type SETANIMATION = "SET_ANIMATION";
type SETRANK = "SET_RANK";
type RESETDISPLAY = 'RESET_DISPLAY';

interface ISETANIMATION {
    type : SETANIMATION,
    display : boolean[]
}

interface ISETRANK {
    type: SETRANK,
    display: boolean
}

interface IRESETDISPLAY {
    type : RESETDISPLAY
}

export type DisplayAction = ISETANIMATION | ISETRANK | IRESETDISPLAY;

export const getDisplayState = () : ThunkResult<void> => {
    return (dispatch : Dispatch<DisplayAction>) => {
        clientSocket.on("displayAnimation", (display : boolean[]) => {
            dispatch(setAnimation(display));
        })    
        clientSocket.on("displayRank", (display : boolean) => {
            dispatch(setRank(display));
        })
        clientSocket.on("ResetDisplay",() => {
            dispatch(resetDisplay())
        })
    }
}

export const setAnimation = (display : boolean[]) : ISETANIMATION => {
    return {
        type : "SET_ANIMATION",
        display
    }
}

export const setRank = (display: boolean): ISETRANK => {
    return {
        type: "SET_RANK",
        display
    }
}

export const resetDisplay = () : IRESETDISPLAY => {
    return {
        type : "RESET_DISPLAY"
    }
}


