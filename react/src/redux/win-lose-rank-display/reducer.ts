import IWinLoseRankDisplayState from './state';

import { DisplayAction } from './action';

const initialState : IWinLoseRankDisplayState = {
    winDisplay : false,
    loseDisplay : false,
    rankDisplay : false
}

// get the ranking from server
const displayReducer = (oldState : IWinLoseRankDisplayState = initialState, action: DisplayAction) 
    : IWinLoseRankDisplayState => {
    switch (action.type) {
        // display or undisplay the winning and losing animation
        case "SET_ANIMATION" : {
            const newState : IWinLoseRankDisplayState = 
                Object.assign({},oldState,{winDisplay : action.display[0], loseDisplay : action.display[1]});
            return newState;
        }
        // display or undisplay the ranking
        case "SET_RANK" : {
            const newState : IWinLoseRankDisplayState = 
                Object.assign({},oldState,{rankDisplay : action.display});
            return newState;
        }
        // reset all display value 
        case "RESET_DISPLAY" : {
            return initialState;
        }
        default : 
            return oldState;
    }
}

export default displayReducer;


