interface IWinLoseRankDisplayState {
    winDisplay : boolean,
    loseDisplay : boolean,
    rankDisplay : boolean
}

export default IWinLoseRankDisplayState