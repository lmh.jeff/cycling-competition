import { ThunkResult } from '../store';
import { clientSocket } from '../../App/App';
import { Dispatch } from 'redux';

type START_THE_GAME = "START_THE_GAME";
type PREPARE_SECOND = "PREPARE_SECOND";
type COUNT_DOWN = "COUNT_DOWN";
type RESETTIME = "RESET_TIME";

interface ISTARTGAME {
    type : START_THE_GAME,
    hasStart : boolean
}

interface IPREPARESECOND {
    type : PREPARE_SECOND,
    prepareSecond : number
}

interface ICOUNTDOWN {
    type : COUNT_DOWN
    second : number
}

interface IRESETTIME {
    type : RESETTIME,
}

export type TimeAction = IPREPARESECOND | ICOUNTDOWN | ISTARTGAME | IRESETTIME;

export const getTimeState = () : ThunkResult<void> => {
    return (dispatch : Dispatch<TimeAction>) => {
        clientSocket.on("PressStart", (hasStart : boolean) => {
            dispatch(startTheGame(hasStart));
        })    
        clientSocket.on("PrepareSecond", (prepareSecond : number) => {
            dispatch(setPrepareSecond(prepareSecond));
        })
        clientSocket.on("CountDown", (second : number) => {
            dispatch(setCountDownSecond(second));
        })
        clientSocket.on("ResetTime", () => {
            dispatch(resetTime());
        })
    }
}

// inform the server to start the game
export const pressStartToServer = () => {
    clientSocket.emit("PressStart"); // press the start button and inform the server
}

// the 3 second prepare before the real count down 
export const prepareToServer = () => {
    clientSocket.emit("PrepareSecond"); // press the start button and inform the server
}

// the server start countdown
export const countDownToServer = () => {
    clientSocket.emit("CountDown"); // press the start button and inform the server
}

// inform the server to restart the state 
export const pressResetToServer = () => {
    clientSocket.emit("PressReset"); // press the start button and inform the server
}

export const startTheGame = (hasStart : boolean) : ISTARTGAME => {
    return {
        type : "START_THE_GAME",
        hasStart
    }
}

export const setPrepareSecond = (prepareSecond : number) : IPREPARESECOND => {
    return {
        type : "PREPARE_SECOND",
        prepareSecond
    }
}

export const setCountDownSecond = (second : number) : ICOUNTDOWN => {
    return {
        type : "COUNT_DOWN",
        second
    }
}

export const resetTime = () : IRESETTIME => {
    return {
        type : "RESET_TIME",
    }
}

