interface ITimeState { 
    hasStart : boolean;
    prepareSecond : number;
    second : number;
}

export default ITimeState;