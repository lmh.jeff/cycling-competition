import ITimeState from './state';
import { TimeAction } from './action';


const initialState : ITimeState = {
    hasStart : false,
    prepareSecond : 3,
    second : 60
}

const timeReducer = (oldState : ITimeState = initialState, action: TimeAction) : ITimeState => {
    switch (action.type) {
        // press the start button
        case "START_THE_GAME" : {
            const newState : ITimeState = Object.assign({},oldState,{ hasStart : action.hasStart });
            return newState;
        }
        // update the prepare second 
        case "PREPARE_SECOND" : {
            const newState : ITimeState = Object.assign({},oldState,{ prepareSecond : action.prepareSecond });
            return newState;
        }
        // update the count down second 
        case "COUNT_DOWN" : {
            const newState : ITimeState = Object.assign({},oldState,{ second : action.second });
            return newState;
        }
        // restart all state
        case "RESET_TIME" : {
            return initialState;
        }
        default : 
            return oldState;
    }
}

export default timeReducer;


