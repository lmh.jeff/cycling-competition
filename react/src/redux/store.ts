import ITimeState from './time/state';
import IDistanceState from './distance/state';
import IRankingState from './Ranking/state';
import IWinLoseRankDisplayState from './win-lose-rank-display/state';

import { TimeAction } from './time/action';
import { DistanceAction } from './distance/actions';
import { RankingAction } from './Ranking/actions';
import { DisplayAction } from './win-lose-rank-display/action';

import timeReducer from './time/reducer';
import distanceReducer from './distance/reducers';
import rankingReducer from './Ranking/reducers';
import displayReducer from './win-lose-rank-display/reducer';

import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import logger from 'redux-logger';
import thunk,{ThunkDispatch,ThunkAction} from 'redux-thunk';





// root state, action and reducer
export interface IRootState {
    time : ITimeState,
    distance: IDistanceState;
    ranking : IRankingState;
    display : IWinLoseRankDisplayState
}

type IRootAction = TimeAction | DistanceAction | RankingAction | DisplayAction; 

export const rootReducer = combineReducers<IRootState>({
    time : timeReducer,
    distance: distanceReducer,
    ranking: rankingReducer,
    display : displayReducer
})

// thunk
export type ThunkResult<R> = ThunkAction<R, IRootState, null, IRootAction>
export type ThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>


// plug in and create store


export default createStore<IRootState,IRootAction,{},{}>(rootReducer,
    compose(
        applyMiddleware(logger),
        applyMiddleware(thunk),
       
    )
)