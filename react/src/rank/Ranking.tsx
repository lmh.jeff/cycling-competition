import * as React from 'react';
import { connect } from 'react-redux';
import { IRootState, ThunkDispatch } from 'src/redux/store';
import { getRanking } from 'src/redux/Ranking/actions';
import * as Moment from 'moment';
import './ranking.css'
import IRankingState, { ISingleRanking, Player1Or2 } from 'src/redux/Ranking/state';
import { getDisplayState } from 'src/redux/win-lose-rank-display/action';
import IWinLoseRankDisplayState from 'src/redux/win-lose-rank-display/state';
import IDistanceState from 'src/redux/distance/state';

interface IRankingProps {
    rank : IRankingState,
    distance : IDistanceState,
    player : Player1Or2 | null
    display : IWinLoseRankDisplayState
    getRanking : () => void
    getDisplayState : () => void
}


class Ranking extends React.Component<IRankingProps> {
    constructor(props: IRankingProps) {
        super(props);
    }

    public componentDidMount() {
        this.props.getRanking();
        this.props.getDisplayState();
    }

    public render() {

        const rankingList : ISingleRanking[] | null = this.getRankList();

        return (
            <div id="rank" className={this.props.display.rankDisplay ? "" : "displayNothing"}>
                <table>
                    <caption>TOP 10 PLAYER</caption>
                    <thead>
                        <tr>
                            <th>PlayerID</th>
                            <th>Distance</th>
                            <th>Date</th>
                            <th>Ranking</th>
                        </tr>
                    </thead>
                    <tbody>
                        {   
                            (rankingList) ?
                                rankingList.map((ranking, i) => {
                                    return (
                                        <tr key={i} className="eachRank">
                                            <td className={this.getRowClassName(ranking.PlayerId)}>
                                                {ranking.PlayerId}
                                            </td>
                                            <td className={this.getRowClassName(ranking.PlayerId)}>
                                                {ranking.Distance}
                                            </td>
                                            <td className={this.getRowClassName(ranking.PlayerId)}>
                                                {Moment(ranking.Date).format('YYYY-MM-DD HH:mm')}
                                            </td>
                                            <td className={this.getRowClassName(ranking.PlayerId)}>
                                                {ranking.Rank}
                                            </td>
                                        </tr>
                                    )
                                }) : null
                        }
                    </tbody>
                </table>
            </div>
        )
    }

    // get the playerid
    private getPlayerId = () : number | null => {
        switch(this.props.player){
            case Player1Or2.Player1 : return this.props.distance.Player1Id;
            case Player1Or2.Player2 : return this.props.distance.Player2Id;
            default : return null;
        }
    }

    // if the table row is the current player, make the text red
    private getRowClassName = (rowId : number) : string => {
        const playerId : number | null = this.getPlayerId();
        if(playerId === rowId){
            return "current-player-rank";
        }
        else{
            return "";
        }
    }

    // get the rank list according to player
    private getRankList = () : ISingleRanking[] | null => {
        switch(this.props.player){
            case Player1Or2.Player1 : return this.props.rank.player1Ranking;
            case Player1Or2.Player2 : return this.props.rank.player2Ranking;
            default : return null;
        }
    }
}


const mapStateToProps = (state: IRootState) => ({
    rank : state.ranking,
    distance : state.distance,
    display : state.display
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    getRanking: () => dispatch(getRanking()),
    getDisplayState : () => dispatch(getDisplayState())
});

export default connect(mapStateToProps, mapDispatchToProps)(Ranking)