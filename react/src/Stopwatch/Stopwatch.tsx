import * as React from 'react';
import { Button, Modal, ModalBody } from 'reactstrap';
import { connect } from 'react-redux';
import { prepareToServer, pressStartToServer, countDownToServer, pressResetToServer, getTimeState } from '../redux/time/action';
import './Stopwatch.css';
import ITimeState from 'src/redux/time/state';
import { IRootState, ThunkDispatch } from 'src/redux/store';

interface IStopwatch {
	time: ITimeState
	getTimeState: () => void
	prepareToServer: () => void
	pressStartToServer: () => void
	countDownToServer: () => void
	pressResetToServer: () => void
}

class Stopwatch extends React.Component<IStopwatch, {}> {

	constructor(props: IStopwatch) {
		super(props);
	}

	// start listening the socket
	public componentDidMount() {
		this.props.getTimeState();
	}

	public render() {
		return (
			<div id="Stopwatch">
				<Modal isOpen={this.isModalOpen()}>
					<ModalBody>{this.props.time.prepareSecond}</ModalBody>
				</Modal>
				<div id="Stopwatch-Time">
					<div>{this.getTimeText(this.props.time.second)}</div>
				</div>
				<div id="Stopwatch-Button">
					<div className="stopWatchButton"><Button onClick={this.props.pressStartToServer}>Start</Button></div>
					<div className="stopWatchButton"><Button onClick={this.props.pressResetToServer}>Reset</Button></div>
				</div>
			</div>
		)
	}

	// start countdown when the game has start
	public componentDidUpdate() {
		// after click the start button
		if (this.props.time.hasStart && this.props.time.second === 60) {
			// triggle the prepare second modal 
			if (this.props.time.prepareSecond === 3) {
				this.props.prepareToServer();
			}
			// then the real count down
			if (this.props.time.prepareSecond <= 0) {
				this.props.countDownToServer();
			}
		}
	}

	private getTimeText = (second: number): string => {
		if (second === 60) {
			return "1:00";
		}
		else if (second >= 10) {
			return `0:${second}`;
		}
		else {
			return `0:0${second}`;
		}
	}

	private isModalOpen = (): boolean => {
		if (this.props.time.hasStart) {
			if (this.props.time.prepareSecond > 0) {
				return true;
			}
		}
		return false;
	}
}

const mapStateToProps = (state: IRootState) => ({
	time: state.time
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
	getTimeState: () => dispatch(getTimeState()),
	prepareToServer: () => prepareToServer(),
	pressStartToServer: () => pressStartToServer(),
	countDownToServer: () => countDownToServer(),
	pressResetToServer: () => pressResetToServer()
});

export default connect(mapStateToProps, mapDispatchToProps)(Stopwatch);
