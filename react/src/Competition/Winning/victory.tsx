import * as React from 'react';
import VictoryAnimation from './victory-animation';
import './victory.css';
import IWinLoseRankDisplayState from 'src/redux/win-lose-rank-display/state';
import { IRootState, ThunkDispatch } from 'src/redux/store';
import { connect } from 'react-redux';
import { Player1Or2 } from 'src/redux/Ranking/state';
import { getDisplayState } from 'src/redux/win-lose-rank-display/action';

interface IVictory {
    player : Player1Or2 | null;
    winner : Player1Or2 | null;
    display : IWinLoseRankDisplayState;
    getDisplayState : () => void
}

class Victory extends React.Component<IVictory,{}> {

    constructor(props: IVictory) {
        super(props);
    }

    public componentDidMount(){
        this.props.getDisplayState();
    }

    public render() {
        return (
            <div id="victory" className={this.shouldDisplay() ? "" : "displayNothing"}>
                <h1>Congratulation! You win!</h1>
                <VictoryAnimation />
            </div>
        )
    }

    // determine should the animation be displayed
    private shouldDisplay = () : boolean => {
        if(this.props.display.winDisplay && this.props.player === this.props.winner){
            return true;
        }
        else{
            return false;
        }
    }
}

const mapStateToProps = (state: IRootState) => ({
	display: state.display
})

const mapDispatchToProps = (dispatch : ThunkDispatch) => ({
    getDisplayState : () => dispatch(getDisplayState())
});

export default connect(mapStateToProps, mapDispatchToProps)(Victory);
