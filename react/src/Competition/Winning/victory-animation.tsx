import * as React from 'react';
import Lottie from 'react-lottie';

class VictoryAnimation extends React.Component {

    public render() {

        const defaultOptions = {
            loop: true,
            autoplay: true,
            animationData: require('./victory.json'),
        };
    
        return (
            <div id="victory-animation">
                <Lottie options={defaultOptions}
                height={905}
                width={1500}
                isClickToPauseDisabled={true} />
            </div>
        )
    }
}

export default VictoryAnimation;