import * as React from 'react';
import './Competition.css';
import ITimeState from '../redux/time/state';
import { IRootState, ThunkDispatch } from '../redux/store';
import { getTimeState } from '../redux/time/action';
import { connect } from 'react-redux';
import Player1 from './Player/player1'
import Player2 from './Player/player2'
import { Modal } from 'reactstrap';
import ModalBody from 'reactstrap/lib/ModalBody';
import { requestPlayerId, getDistanceState } from 'src/redux/distance/actions';
import IDistanceState from 'src/redux/distance/state';
import Victory from './Winning/victory';
import Losing from './Losing/losing';
import Ranking from '../rank/Ranking';
import { Player1Or2 } from '../redux/Ranking/state';
import IWinLoseRankDisplayState from 'src/redux/win-lose-rank-display/state';

interface ICompetition {
    time : ITimeState,
    distance : IDistanceState,
    display : IWinLoseRankDisplayState

    getTimeState : () => void
    getDistanceState : () => void
    requestPlayerId : () => void

    match : {
        params : {
            player : string
        }
    }
}

class Competition extends React.Component<ICompetition> {

    private player : Player1Or2 | null;

    public constructor(props: ICompetition) {
        super(props);
        const paramPlayer : string = props.match.params.player;
        // only accept player 1 or 2
        switch (paramPlayer) {
            case "1" : this.player = Player1Or2.Player1; break;
            case "2" : this.player = Player1Or2.Player2; break;
            default : this.player = null;
        }
        this.state = {
            displayVictory: false
        }
    }

    // start listening the socket
    public componentDidMount() {
        this.props.getTimeState();
        this.props.requestPlayerId();
        this.props.getDistanceState();
    }

    public render() {
        return (
            <div id="competition">
                <Victory player={this.player} winner={this.getWinner()} />
                <Losing player={this.player} winner={this.getWinner()} />
                <Ranking player={this.player}/>
                <Modal isOpen={this.isModalOpen()}>
                    <ModalBody>{this.props.time.prepareSecond}</ModalBody>
                </Modal>
                {/*show each player id and distance*/}
                <div id="info-display" className={this.getInfoClass()}>
                    <div id="time-display">
                        {this.getTimeText(this.props.time.second)}
                    </div>
                    <div id="id-display">
                        Player : {this.getPlayerId()}
                    </div>
                    <div id="distance-display">
                        Distance : {this.getDistance()}m
                    </div>
                </div>
                {/*show the road track animation*/}
                <div id="roadMap" className={this.getRoadClass()}>
                    <Player1 />
                    <Player2 />
                </div>
            </div>
        )
    }

    private getWinner = () : Player1Or2 | null => {
        // the game hasn't finished, no winner
        if(this.props.time.second !== 0){
            return null;
        }
        else {
            const winner : Player1Or2 = 
                (this.props.distance.Player1TotalDistance > this.props.distance.Player2TotalDistance) ?
                Player1Or2.Player1 : Player1Or2.Player2;
            return winner;
        }
    }

    private getTimeText = (second: number): string => {
        if (second === 60) {
            return "1:00";
        }
        else if (second >= 10) {
            return `0:${second}`;
        }
        else {
            return `0:0${second}`;
        }
    }

    private isModalOpen = (): boolean => {
        if (this.props.time.hasStart) {
            if (this.props.time.prepareSecond > 0) {
                return true;
            }
        }
        return false;
    }

    // set the class name to change the info text color and the opacity 
    private getInfoClass = () : string => {
        let classString = "";
        // red color for player 1 and blue color for player 2
        switch(this.player){
            case Player1Or2.Player1 : classString += " player1-color"; break;
            case Player1Or2.Player2 : classString += " player2-color"; break;
        }
        // when the game finishs, transparent the background
        if(this.props.time.second === 0){
            classString += " competition-finish";
        }
        return classString;
    }

    // get the playerid
    private getPlayerId = () : number | null => {
        switch(this.player){
            case Player1Or2.Player1 : return this.props.distance.Player1Id;
            case Player1Or2.Player2 : return this.props.distance.Player2Id;
            default : return null;
        }
    }

    // get the distance
    private getDistance = () : number | null => {
        switch(this.player){
            case Player1Or2.Player1 : return this.props.distance.Player1TotalDistance;
            case Player1Or2.Player2 : return this.props.distance.Player2TotalDistance;
            default : return null;
        }
    }

    // set the class name to change the opacity
    private getRoadClass = () : string => {
        let classString = "";
        if(this.props.time.second === 0){
            classString += " competition-finish";
        }
        return classString;
    }
}

const mapStateToProps = (state: IRootState) => ({
    time : state.time,
    distance : state.distance,
    display : state.display
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    getTimeState : () => dispatch(getTimeState()),
    requestPlayerId : () => requestPlayerId(),
    getDistanceState : () => dispatch(getDistanceState())
});

export default connect(mapStateToProps, mapDispatchToProps)(Competition);
