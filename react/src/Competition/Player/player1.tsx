import * as React from 'react';
import Lottie from 'react-lottie';
import './player.css';
import IDistanceState from 'src/redux/distance/state';
import { IRootState } from 'src/redux/store';
import { connect } from 'react-redux';

interface IPlayer {
    distance : IDistanceState
}

class Player1 extends React.Component<IPlayer,{}> {

    constructor(props : IPlayer) {
        super(props);
    }

    public render() {

        const defaultOptions = {
            loop: true,
            autoplay: true,
            animationData: require('./player1.json'),
        };

        return (
            <div id="player1">
                <Lottie options={defaultOptions}
                    height={700}
                    width={`1500px`}
                    speed={this.props.distance.Player1Speed}
                    isStopped={this.props.distance.isStopped} />
            </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => ({
    time : state.time,
    distance : state.distance
})

export default connect(mapStateToProps, {})(Player1);
