import * as React from 'react';
import Lottie from 'react-lottie';
import './player.css';
import IDistanceState from 'src/redux/distance/state';
import { IRootState } from 'src/redux/store';
import { connect } from 'react-redux';

interface IPlayer {
    distance : IDistanceState
}

class Player2 extends React.Component<IPlayer,{}> {

    constructor(props : IPlayer) {
        super(props);
    }

    public render() {

        const defaultOptions = {
            loop: true,
            autoplay: true,
            animationData: require('./player2.json'),
        };



        return (
            <div id="player2">
                <Lottie options={defaultOptions}
                    height={700}
                    width={`100vw`}
                    speed={this.props.distance.Player2Speed}
                    isStopped={this.props.distance.isStopped}
                    isClickToPauseDisabled={true} />
            </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => ({
    time : state.time,
    distance : state.distance
})

export default connect(mapStateToProps, {})(Player2);
