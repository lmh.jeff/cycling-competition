import * as React from 'react';
import LosingAnimation from './losing-animation';
import './losing.css';
import { Player1Or2 } from 'src/redux/Ranking/state';
import IWinLoseRankDisplayState from 'src/redux/win-lose-rank-display/state';
import { IRootState, ThunkDispatch } from 'src/redux/store';
import { connect } from 'react-redux';
import { getDisplayState } from 'src/redux/win-lose-rank-display/action';

interface ILosing {
    player: Player1Or2 | null;
    winner: Player1Or2 | null;
    display: IWinLoseRankDisplayState;
    getDisplayState: () => void
}

class Losing extends React.Component<ILosing, {}> {

    constructor(props: ILosing) {
        super(props);
    }

    public componentDidMount() {
        this.props.getDisplayState();
    }

    public render() {
        return (
            <div id="losing" className={this.shouldDisplay() ? "" : "displayNothing"}>
                <div id="losingH1">Sorry! Please try again!</div>
                <div id="whiteFaceOuter">
                    <div id="white-face">{/*Animation White Face Background*/}<LosingAnimation /></div>                
                </div>
            </div>
        ) 
    }

    // determine should the animation be displayed
    private shouldDisplay = () : boolean => {
        if(this.props.display.loseDisplay && this.props.player !== this.props.winner){
             return true; 
        }
        else{
             return false;
        }
    }
}

const mapStateToProps = (state: IRootState) => ({
	display: state.display
});

const mapDispatchToProps = (dispatch : ThunkDispatch) => ({
    getDisplayState : () => dispatch(getDisplayState())
});

export default connect(mapStateToProps, mapDispatchToProps)(Losing);