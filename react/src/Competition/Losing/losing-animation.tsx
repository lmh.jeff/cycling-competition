import * as React from 'react'
import Lottie from 'react-lottie';

class LosingAnimation extends React.Component {

    public render() {

        const defaultOptions = {
            loop: true,
            autoplay: true,
            animationData: require('./losing.json'),
        };
    
        return (
            <div id="losing-animation">
                <Lottie options={defaultOptions}
                    height={800}
                    width={800}
                    isClickToPauseDisabled={true} />
            </div>
        )
    }
}

export default LosingAnimation;