// Load Wi-Fi library
#include <WiFi.h>
#include <HTTPClient.h>

// Replace with your network credentials
const char* ssid     = "Heichen_Slow";
const char* password = "toproofhk2012";
const char* serverAddress = "http://10.57.17.106:8080/";

int meters = 0;

HTTPClient http;

// variables for esp and sensor display
// variables for the sensors display
int previousSpinA = -1;
int previousSpinB = -1;
int spinA = 0;
int spinB = 0;
int previousInterruptMsA = 0;
int previousInterruptMsB = 0;

// variables for fans
int enaPin = 12; // for speed control
int enbPin = 14; // for speed control
int n1Pin = 27;
int n2Pin = 33;
int n3Pin = 15;
int n4Pin = 32 ;
int enaChannel = 0 ;
int enbChannel = 1;
double frequency = 16000; // floating point numbers 16000
int resolution = 8;


void setup() {
  Serial.begin(115200);
  //  set up pinMode 26 to be an INPUT
  pinMode(26, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(26), addSpinA, RISING);

  pinMode(25, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(25), addSpinB, RISING);

  // set up for the fans

  pinMode (n1Pin, OUTPUT);
  pinMode (n2Pin, OUTPUT);
  pinMode (n3Pin, OUTPUT);
  pinMode (n4Pin, OUTPUT);


  digitalWrite(n1Pin, HIGH); // use the red wire in the motor controller
  digitalWrite(n2Pin, LOW);
  digitalWrite(n3Pin, LOW);
  digitalWrite(n4Pin, HIGH);

  ledcSetup (enaChannel, frequency, resolution);
  ledcSetup (enbChannel, frequency, resolution);

  ledcAttachPin (enaPin, enaChannel);
  ledcAttachPin (enbPin, enbChannel);


  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  http.setReuse(true);
  http.begin(serverAddress);
  http.addHeader("Content-Type", "text/plain");
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

}


void loop() {
  float speedA = 3.6 * 1.225 * (spinA - previousSpinA);

  ledcWrite(enaChannel, getFanSpeed(speedA));

  Serial.print ("SpeedA: ");
  Serial.println(speedA);

  if (previousSpinA != spinA) {
    Serial.print("A: ");
    Serial.println(spinA);
    previousSpinA = spinA;
  };

  float speedB = 3.6 * 1.225 * (spinB - previousSpinB);

  ledcWrite(enbChannel, getFanSpeed(speedB));

  Serial.print ("                SpeedB: ");
  Serial.println(speedB);

  if (previousSpinB != spinB) {
    Serial.print("                B: ");
    Serial.println(spinB);
    previousSpinB = spinB;
  }

  int httpResponseCode = http.POST(
    "[" + String(spinA) + "," + String(spinB) + "]"
  );

  if (httpResponseCode > 0) {

  } else {

    Serial.print("Error on sending POST: ");
    Serial.println(httpResponseCode);
  }
  
  delay(1000);
}

int getFanSpeed (float speed) {

  int fanSpeed = 0;

  if (speed > 0 ) {
    fanSpeed = 10;
  }
  if (speed > 15) {
    fanSpeed = 150;
  }
  if (speed > 30) {
    fanSpeed = 255;
  }

  return fanSpeed;
}

void addSpinA () {
  int t = millis();
  // Treat anything faster than 100 RPM as noisy sensor
  // 100/s * 1.22m = 122 m/s = 439 km/h
  if (t - previousInterruptMsA >= 10) {
    spinA++;
  }
  previousInterruptMsA = t;
}

void addSpinB () {
  int t = millis();
  // Treat anything faster than 100 RPM as noisy sensor
  if (t - previousInterruptMsB >= 10) {
    spinB++;
  }
  previousInterruptMsB = t;
}