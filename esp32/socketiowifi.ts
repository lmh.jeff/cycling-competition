
// this file is added for if uses wifi for connection, 
import * as openSocket from "socket.io-client";


let readyToSend = false;

const clientSocket = openSocket('http://192.168.1.170:8080');

clientSocket.on('connect', function () {
    clientSocket.emit("requesttosenddata")
    readyToSend = true;
});

clientSocket.on('disconnect', function () {
    readyToSend = false;
});