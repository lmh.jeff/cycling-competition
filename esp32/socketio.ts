import * as openSocket from "socket.io-client";
import * as SerialPort from 'serialport';
import * as readline from 'readline';


// before it is connected, it is not ready to send, thus it is false. 
let readyToSend = false;
const wheelSize = 1.225;

let player1RaceStartDistance = -1;
let player2RaceStartDistance = -1;

interface IESP {
    player1Distance: number,
    player2Distance: number,
    player1Speed: number,
    player2Speed: number,
}

const serverRequest: IESP = {
    player1Distance: -1,
    player2Distance: -1,
    player1Speed: 0,
    player2Speed: 0
};

// keep track the latest distance ; 
// what is the current distance 1000 
// new distance - current distance

const clientSocket = openSocket(`https://api.speedriding.club`);

clientSocket.on('connect', function () {
    clientSocket.emit("requesttosenddata")
    readyToSend = true;
});

clientSocket.on('disconnect', function () {
    readyToSend = false;
});

clientSocket.on("Reset", () => {
    player1RaceStartDistance = serverRequest.player1Distance;
    player2RaceStartDistance = serverRequest.player2Distance;
})


// how i receive a mesaage from terry , using a vaiable 
// create a variavle at the socket on 

const port = new SerialPort('/dev/cu.SLAB_USBtoUART', {
    baudRate: 115200
})

port.on('error', function (err) {
    console.log('Error: ', err.message)
})


function distance(spin: number, wheelSize: number) {
    return spin * wheelSize
}

const rl = readline.createInterface({
    input: port as any,
    crlfDelay: Infinity
});

rl.on('line', (line: string) => {
    console.log(`ESP Data ${line}`);
    let [key, value] = line.replace(/\s/g, '').split(":");
    let num = parseFloat(value);

    if (key === "A") {

        serverRequest.player1Distance = distance(num, wheelSize)
        if (player1RaceStartDistance === -1) {
            player1RaceStartDistance = serverRequest.player1Distance
        }
    } else if (key === "B") {
        serverRequest.player2Distance = distance(num, wheelSize)
        if (player2RaceStartDistance === -1) {
            player2RaceStartDistance = serverRequest.player2Distance
        }
    } else if (key === "SpeedA") {
        serverRequest.player1Speed = num;
    } else if (key === "SpeedB") {
        serverRequest.player2Speed = num;
    } else {
        console.log("The data is incorrect, please check")
        return;
    }

    //   filling the data from  vvv from terryrequest
    const serverRequestCopy = { ...serverRequest };
    serverRequestCopy.player1Distance -= player1RaceStartDistance;
    serverRequestCopy.player2Distance -= player2RaceStartDistance;
    console.log(serverRequestCopy);

    if (readyToSend) {
        clientSocket.emit("cyclingdata", serverRequestCopy);
    }
});