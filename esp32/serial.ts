
import * as SerialPort from 'serialport';
import * as readline from 'readline';

//set the variables for the distance calculation 

const wheelSize = 1.225 ; 

interface IDistance {
    A?: number;
    B?: number;
    distanceA?: number; 
    distanceB?: number;
    speedA?:number;
    speedB?:number;
}

// distance calculation formula 
function distance (spin:number, wheelSize: number) {
    return spin * wheelSize
}

// open the port 
const port = new SerialPort ('/dev/cu.SLAB_USBtoUART',{
    baudRate: 115200
})

// Open errors will be emitted as an error event
port.on('error', function(err) {
    console.log('Error: ', err.message)
})

// Read from the serial port line-by-line
const rl = readline.createInterface({
  input: port,
  crlfDelay: Infinity
});

/*
    Lines are of format:
        "spinA: <num>"
        "speedA: <num>"
        "              spinB: <num>"
        "              speedB: <num>"

    We convert them to objects like:
        {spinA: <num>, distanceA: <num>}
        {speedA: <num>}
        {spinB: <num>, distanceB: <num>}
        {speedB: <num>}

*/
rl.on('line', (line) => {
    console.log(`ESP DATA: ${line}`);
let obj:IDistance = {};

let [key, value] = line.replace(/\s/g, '').split(":");
let num = parseFloat(value);
obj [key] = num;

// The startsWith() method determines whether a string begins with the characters of a specified string, 
// returning true or false as appropriate.

if (key === 'A' || key === 'B') {
    let d = distance (obj[key], wheelSize);
    obj[key] = d; 
}

console.log (obj)

});


