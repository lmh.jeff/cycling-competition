
int previousSpinA = -1;
int previousSpinB = -1;
int spinA = 0;
int spinB = 0;
int previousInterruptMsA = 0;
int previousInterruptMsB = 0;


const int enaPin = 12; // for speed control
const int enbPin = 14; // for speed control in the MD it is enb and esp's GPIO 14/A6
const int n1Pin = 27; // ledPin refers to ESP32 GPIO 27/A10 
const int n2Pin = 33; // in the MD (N2) and In the esp GPIO 33/A9 
const int n3Pin = 15; // in the MD (N3) and in the esp GPIO 15/A8 
const int n4Pin = 32 ; // in the MD (N4) and inthe esp GPIO 32/A7 
const int enaChannel = 0 ; // write whatever channel from 0 to 15 ( 16 channels for ESP) 
const int enbChannel = 1;   // write from 0 - 15 channels, led teams 
const double frequency = 16000; // 16000 Hertz / 1 second, this is given by testing the fan noise  how fast the ledcontroller switch on / off 
const int resolution = 8; // 8 bits ...the resolution controls how many different speed the fan can have between stopped and full speed 


void setup() {
  Serial.begin(115200); /
  pinMode(26, INPUT_PULLUP); 
  attachInterrupt(digitalPinToInterrupt(26), addSpinA, RISING);

  pinMode(25, INPUT_PULLUP); 
  attachInterrupt(digitalPinToInterrupt(25), addSpinB, RISING);

  // set up for the fans

  pinMode (n1Pin, OUTPUT); 
  pinMode (n2Pin, OUTPUT); /
  pinMode (n3Pin, OUTPUT);
  pinMode (n4Pin, OUTPUT);
  

  digitalWrite(n1Pin, HIGH); // use the red wire in the motor controller and no frequency 
  digitalWrite(n2Pin, LOW); // turn the led off by marking the voltage low 
  digitalWrite(n3Pin, LOW);
  digitalWrite(n4Pin, HIGH); // turn the led on (high is the voltage level)


  ledcSetup (enaChannel, frequency, resolution);
  ledcSetup (enbChannel, frequency, resolution);

  ledcAttachPin (enaPin, enaChannel);
  ledcAttachPin (enbPin, enbChannel);
}

  
void loop() {
  float speedA = 3.6 * 1.225 * (spinA - previousSpinA);
  // 1.225 / 1000  convert to km 
  // 1.225 / 1000 / (1/3600)  + conver to hour 
  // 1 / 1000 ( 1/3600) = 3.6  per second per km 
  // float is for decimal number and it does the calculation in the float;  int does not have decimal 

  ledcWrite(enaChannel, getFanSpeed(speedA)); // how much the time spend high and low 

//  Serial.print ("SpeedA: "); // print out whatever it is in 
//  Serial.println(speedA); // convert a number to string

  Serial.println ("SpeedA:  " +  String (speedA)); 

  
  if (previousSpinA != spinA) {
    Serial.print("A: ");
    Serial.println(spinA);
    previousSpinA = spinA;
  };

  float speedB = 3.6 * 1.225 * (spinB - previousSpinB);

  ledcWrite(enbChannel, getFanSpeed(speedB));

  Serial.print ("                     SpeedB: ");
  Serial.println(speedB);

  if (previousSpinB != spinB) {
    Serial.print("                    B: ");
    Serial.println(spinB);
    previousSpinB = spinB;
  }
  delay(1000 ); // wait for a second 
}

int getFanSpeed (float speed) {

  int fanSpeed = 0;

  if (speed > 0 ) {
    fanSpeed = 10;
  }
  if (speed > 15.0) {
    fanSpeed = 150;
  }
  if (speed > 30.0) {
    fanSpeed = 255;
  }

  return fanSpeed;
}

void addSpinA () {
  int t = millis();
  // Treat anything faster than 25 RPM as noisy sensor
  // 25/s * 1.22m = 110 km/h
  if (t - previousInterruptMsA >= 40) {
    spinA++;
  }
  previousInterruptMsA = t;
}

void addSpinB () {
  int t = millis();
  // Treat anything faster than 25 RPM as noisy sensor
  if (t - previousInterruptMsB >= 40) {
    spinB++;
  }
  previousInterruptMsB = t;
}