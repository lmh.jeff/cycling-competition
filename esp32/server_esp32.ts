// this is only testing file and for receiving esp data via wifi set up code

import * as express from 'express';
import {Request,Response} from 'express';


const app = express();

app.get('/',function(req:Request,res:Response){
    console.log ("got a request")
    res.end("Hello World");
})

app.post ('/',function(req:Request,res:Response){
    const chunks:Buffer[] = [];
    req.on('data', (chunk) => {
        chunks.push(chunk); 
    });
    req.on('end', () => {
        const body = Buffer.concat(chunks);
        process.stdout.write("\r");
        process.stdout.write(body);
        process.stdout.write("          ");
        res.end("1 min counting down");
    });

})

const PORT = 8080;

app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
})