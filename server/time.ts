import { timeState, distanceState, displayState } from './init-value';
import Background from './background';
import Database from './database';

class Time {

    private serverBoardCast: SocketIO.Server
    constructor(serverBoardCast: SocketIO.Server) {
        this.serverBoardCast = serverBoardCast;
    }

    pressStart = () => {
        // only start the game the game hasn't start yet
        if (!timeState.hasStart) {
            timeState.hasStart = true;
            this.serverBoardCast.to("React").emit("PressStart", timeState.hasStart);
        }

    }

    prepareCountDown = () => {
        const prepareInterval = setInterval(() => {
            timeState.prepareSecond--;
            this.serverBoardCast.to("React").emit("PrepareSecond", timeState.prepareSecond);
            // clear the counter when the time goes to 0 or disconnect\
            if (timeState.prepareSecond <= 0) {
                
                
                clearInterval(prepareInterval);
            }
        }, 1000);
    }

    countDown = () => {
        this.serverBoardCast.to("ESP").emit("Reset");
        timeState.timeInterval = setInterval(() => {
            timeState.second--;
            this.serverBoardCast.to("React").emit("CountDown", timeState.second);

            // clear the counter when the time goes to 0 or disconnect
            if (timeState.timeInterval && timeState.second <= 0) {
                clearInterval(timeState.timeInterval);
            }
        }, 1000);
    }

    //clear all the value and remove the intervol
    pressReset = async(background: Background, database : Database) => {
        // only reset the game with suitable timing
        if (!background.getResetLock()) {
            // first restart the time value
            timeState.second = 60;
            timeState.prepareSecond = 3;
            timeState.hasStart = false;
            if (timeState.timeInterval) {
                clearInterval(timeState.timeInterval);
            }

            // then the distance value
            distanceState.espData.player1Distance = 0;
            distanceState.espData.player2Distance = 0;
            distanceState.isStopped = false;
            const id : number[] = await database.genid();
            
            // then the display value
            displayState.winDisplay = false;
            displayState.loseDisplay = false;
            displayState.rankDisplay = false;

            //finally reset the background class value
            background.reset();

            this.serverBoardCast.to("React").emit("ResetTime");
            this.serverBoardCast.to("React").emit("ResetDistance");
            this.serverBoardCast.to("React").emit("PlayerId",id);
            this.serverBoardCast.to("React").emit("ResetDisplay");

        }
    }
}

export default Time 
