import { distanceState, timeState, displayState } from "./init-value";
import Database from "./database";
import IRankingState, { ISingleRanking } from "../react/src/redux/Ranking/state";
// import { espState } from "./init-value";

class Background {

    //only do the database and display function once
    private rankingExecute : boolean;
    private displayExecute : boolean;
    private timeInterval : NodeJS.Timeout | null;
    private serverBoardCast : SocketIO.Server;
    private database : Database;
    private resetLock : boolean // cannot reset when the game finish and sending the data to the database

    constructor (serverBoardCast : SocketIO.Server, database : Database) {
        this.rankingExecute = false;
        this.displayExecute = false;
        this.timeInterval = null;
        this.serverBoardCast = serverBoardCast;
        this.database = database;
        this.resetLock = false;
    }

    // keep sending the speed and distance per 0.5 second when the game start
    private gameStartSending = () => {
        this.timeInterval = setInterval(() => {
            this.serverBoardCast.to("React").emit("StartCycling");
            // these fake data should be replaced by the esp data
            // distanceState.espData.player1Speed = Math.random()*50;
            // distanceState.espData.player2Speed = Math.random()*50;
            // distanceState.espData.player1Distance += Math.round(distanceState.espData.player1Speed);
            // distanceState.espData.player2Distance += Math.round(distanceState.espData.player2Speed);
            distanceState.isStopped = true;
            this.serverBoardCast.to("React").emit("PlayerSpeed",
                [distanceState.espData.player1Speed/10, distanceState.espData.player2Speed/10]);
            this.serverBoardCast.to("React").emit("PlayerDistance",
                [distanceState.espData.player1Distance.toFixed(2), 
                 distanceState.espData.player2Distance.toFixed(2)]);
            if(this.timeInterval && timeState.second <= 0){ 
                this.serverBoardCast.to("React").emit("PlayerSpeed",[0,0]); // times over! stop all the animation
                clearInterval(this.timeInterval);
            }
        } , 500)
    }

    // add the player 1 and player 2 record into the database
    private addRecord = async() => {
        const player1Record : ISingleRanking  = {
            PlayerId : distanceState.player1Id,
            Distance : distanceState.espData.player1Distance,
            Date : new Date()
        }
        const player2Record : ISingleRanking = {
            PlayerId : distanceState.player2Id,
            Distance : distanceState.espData.player2Distance,
            Date : new Date()
        }
        await this.database.addDatabase(player1Record);
        await this.database.addDatabase(player2Record);
    }

    // get the ranking list with 1-10 ranking and the player himself
    private getRanking = async() : Promise<IRankingState> => {
        const top10WithPlayer1 = await this.database.getTop10AndMyself(distanceState.player1Id);
        const top10WithPlayer2 = await this.database.getTop10AndMyself(distanceState.player2Id);
        const result : IRankingState = {
            player1Ranking : top10WithPlayer1,
            player2Ranking : top10WithPlayer2
        }
        return result;
    }

    // function that will run per second
    public exe = () => {
        setInterval(async()=> {
            // only set the interval when the game start
            if(timeState.second === 60 && timeState.prepareSecond === 0){
                // keep sending the speed and the distance of the bike to react
                this.gameStartSending();
            }
            // when the game finish
            if(timeState.second === 0){
                // from now on, the user cannot reset
                this.resetLock = true;
                // execute some database function
                if(!this.rankingExecute){
                    // add the record
                    await this.addRecord();
                    // get the ranking and then send to the react
                    const result : IRankingState = await this.getRanking();
                    this.serverBoardCast.to("React").emit("rank",result);
                    // only doing this once
                    this.rankingExecute = true;    
                }
                // show the winning and losing animation
                if(!displayState.winDisplay && !displayState.loseDisplay){
                    if(!this.displayExecute){
                        displayState.winDisplay = true;
                        displayState.loseDisplay = true;
                        this.serverBoardCast.to("React").emit("displayAnimation", 
                            [displayState.winDisplay, displayState.loseDisplay]);
                        setTimeout(() => {
                            displayState.winDisplay = false;
                            displayState.loseDisplay = false;
                            this.serverBoardCast.to("React").emit("displayAnimation",
                                [displayState.winDisplay, displayState.loseDisplay]);
                            displayState.rankDisplay = true
                            this.serverBoardCast.to("React").emit("displayRank",displayState.rankDisplay);
                        },5000)
                        // only doing this once
                        this.displayExecute = true;
                    }
                }
            }
            // now the user can click the reset after the ranking list has been displayed
            if(displayState.rankDisplay){
                this.resetLock = false;
            }
        }, 1000)
    }

    // when the reset button is click, reset the value
    public reset = () => {
        this.rankingExecute = false;
        this.displayExecute = false;
        if(this.timeInterval){ 
            clearInterval(this.timeInterval);
        }
    }

    public getResetLock = () : boolean => {
        return this.resetLock;
    }
}

export default Background;






