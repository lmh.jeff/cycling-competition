import * as Knex from "knex";
import { ISingleRanking } from "../../react/src/redux/Ranking/state";

const insertValue : ISingleRanking[] = new Array();

// gen random date for the cycling from 9am to 9pm
const genData = () => {
    let id : number = 0;
    let hour : number = 9;
    let minute : number = 0;
    while(hour != 21){
        insertValue.push({
            PlayerId : id,
            Distance : Math.random()*1000, // 5000 <= distance < 6000
            Date : new Date(2019,1,23,hour,minute)
        })
        // next user
        id +=1;
        if(id % 2 === 0) {
            minute += 1;
            if(minute === 60){
                minute = 0;
                hour+=1;
            }
        }
    }
}

const addList = async(knex : Knex) : Promise<void> => {
    const hasTable : boolean = await knex.schema.hasTable("List");
    if(hasTable){
        await knex("List").del();
        genData();
        await knex.insert(insertValue).into("List");
    }
}

exports.seed = addList;

