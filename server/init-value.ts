import IWinLoseRankDisplayState from "../react/src/redux/win-lose-rank-display/state";

interface ITimeState {
    hasStart : boolean,
    prepareSecond : number,
    second : number
    timeInterval : NodeJS.Timeout | null
}

interface IDistanceState {
    player1Id : number,
    player2Id : number,
    espData : IESP
    isStopped : boolean
}

export interface IESP {
    player1Distance : number,
    player2Distance : number,
    player1Speed : number,
    player2Speed : number
}

export const timeState : ITimeState = {
    hasStart : false,
    prepareSecond : 3,
    second : 60,
    timeInterval : null 
}

export const espState : IESP = {
    player1Distance : 0,
    player2Distance : 0,
    player1Speed : 0,
    player2Speed : 0    
}

export const distanceState : IDistanceState = {
    player1Id : 0,
    player2Id : 0,
    espData : espState,
    isStopped : false
}

export const displayState : IWinLoseRankDisplayState = {
    winDisplay : false,
    loseDisplay : false,
    rankDisplay : false
} 


