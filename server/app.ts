import * as express from "express";
import * as http from 'http';
import * as socketIo from "socket.io";
import Time from "./time";
import * as Knex from "knex";
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig["development"]);
import Database from "./database";
import Background from "./background";
import { espState } from './init-value';
import { IESP } from './init-value';

const app = express();

const server = new http.Server(app);
const io = socketIo(server);
const database = new Database(knex);
const background = new Background(io,database);

io.on('connection', serverSocket => {
    const time = new Time(io);
    // press the start button
    serverSocket.on("PressStart", () => time.pressStart());
    // prepare countdown before the real count down
    serverSocket.on("PrepareSecond", () => time.prepareCountDown());
    // countdown to 0
    serverSocket.on("CountDown", () => time.countDown());
    // press the reset button
    serverSocket.on("PressReset", () => time.pressReset(background, database));
    // get the player id from the begining
    serverSocket.on("requestPlayerId", async() =>  {
        const id : number[] = await database.genid();
        io.emit("PlayerId",id);
    })
    // assign the esp value to the server 
    serverSocket.on("cyclingdata", (newespState :IESP) => {
        espState.player1Distance = newespState.player1Distance;
        espState.player2Distance = newespState.player2Distance;
        espState.player1Speed = newespState.player1Speed;
        espState.player2Speed = newespState.player2Speed;
        console.log(espState);
    })
    // add a room for the react socket only
    serverSocket.on("ReactRoom", () => {
        console.log("joinReactroom");
        serverSocket.join("React");
    })
    // add a room for the esp socket only
    serverSocket.on("requesttosenddata", () => {
        console.log("joinESProom");
        serverSocket.join("ESP");
    })
});

background.exe();

io.listen(8080);


