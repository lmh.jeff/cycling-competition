import * as Knex from "knex";
import { distanceState } from "./init-value";
import { ISingleRanking } from "../react/src/redux/Ranking/state";

class Database {
    private knex : Knex;
    constructor (knex : Knex){
        this.knex = knex;
    } 

    // get the top 10 player and the target player (himself/herself)
    public getTop10AndMyself = async(playerId : number) : Promise <ISingleRanking[]> => { 
        const result = await this.knex.raw(`
            SELECT "PlayerId", "Distance", "Date", "Rank" FROM (
                SELECT "PlayerId", "Distance", "Date",
                    RANK() OVER (
                        ORDER BY "Distance" DESC
                    ) AS "Rank"
                FROM "List"
            ) as "ranking"
            WHERE "Rank" BETWEEN 1 AND 10 OR "PlayerId" = ${playerId}
            ORDER BY "Distance" DESC, "PlayerId" DESC`
        );
        const top10AndMyself : ISingleRanking[] = result.rows;
        return top10AndMyself;
}

    // generate the player 1 & 2 according to the length of the database
    public genid = async() : Promise <number[]> => {
        const result = await this.knex.raw(`
            SELECT COUNT(*) AS RowNumber
            FROM "List"
        `);
        const rowNumber = parseInt(result.rows[0].rownumber);
        distanceState.player1Id = rowNumber + 1;
        distanceState.player2Id = rowNumber + 2;
        return [distanceState.player1Id,distanceState.player2Id];
    } 

    // add the playerid, distance, date and ranking to the database
    public addDatabase = async(newRecord : ISingleRanking) : Promise <void> => {
        await this.knex.insert(newRecord).into("List");
    }
}

export default Database;



