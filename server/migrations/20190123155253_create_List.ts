import * as Knex from "knex";

const makeList = async(knex : Knex) : Promise <void> => {
    const hasTable : boolean = await knex.schema.hasTable("List");
    if(!hasTable){
        await knex.schema.createTable("List",table=>{
            table.increments();
            table.integer("PlayerId").unsigned().unique().notNullable();
            table.float("Distance").notNullable();
            table.timestamp("Date").notNullable();
        })
    }
}

const destoryList = async(knex : Knex) : Promise <void> => {
    const hasTable : boolean = await knex.schema.hasTable("List");
    if(hasTable){
        await knex.schema.dropTable("List");
    }
}

exports.up = makeList;

exports.down = destoryList;
